//
//  PostsSectionHeaderView.swift
//  RealmCleanArchitectureRx
//
//  Created by Diep Nguyen on 2/9/20.
//  Copyright © 2020 drjoy. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class PostsSectionHeaderView: UIView, BaseViewType {

    var disposeBag = DisposeBag()

    let labelTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()

    let arrowDown: UIImageView = {
        let imgV = UIImageView()
        imgV.layer.masksToBounds = true
        imgV.contentMode = .scaleAspectFit
        imgV.image = UIImage(named: "arrrow_down")?.withRenderingMode(.alwaysOriginal)

        return imgV
    }()

    let buttonExpaned: UIButton = {
        let button = UIButton()
        return button
    }()

    func layout() {
        let separatorLine = UIView()
        separatorLine.backgroundColor = .gray
        [labelTitle, arrowDown, buttonExpaned, separatorLine].forEach(addSubview)

        labelTitle.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
            make.centerY.equalToSuperview()
        }

        arrowDown.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().offset(-15)
            make.width.height.equalTo(15)
            make.centerY.equalToSuperview()
        }

        buttonExpaned.snp.makeConstraints { (make) in
            make.leading.top.equalToSuperview().offset(5)
            make.bottom.trailing.equalToSuperview().offset(-5)
        }

        separatorLine.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
            make.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
    }

    func bind() {

    }
}
