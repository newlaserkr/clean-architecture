//
//  CreatePostViewModel.swift
//  RealmCleanArchitectureRx
//
//  Created by Diep Nguyen on 2/10/20.
//  Copyright © 2020 drjoy. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class CreatePostViewModel: TransformableViewModelType {

    private let useCase: PostsUseCase
    private let navigator: CreatePostNavigator

    init(useCase: PostsUseCase, navigator: CreatePostNavigator) {
        self.useCase = useCase
        self.navigator = navigator
    }

    struct Input {
        let saveTrigger: Driver<Void>
        let cancelTrigger: Driver<Void>
    }

    struct Output {
        let saveNewsPost: Driver<Void>
        let backToPosts: Driver<Void>
    }

    func transform(input: Input) -> Output {

        let saveNewPost = input.saveTrigger.do(onNext: { (_) in
            // 1. save data into Realm
            // 2. back to previous screen
        })

        let backToPosts = input.cancelTrigger.do(onNext: { [weak self](_) in
            self?.navigator.toPosts()
        })
        return Output(saveNewsPost: saveNewPost,
                      backToPosts: backToPosts)
    }

}
