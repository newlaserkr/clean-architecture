//
//  PostsSectionData.swift
//  RealmCleanArchitectureRx
//
//  Created by Diep Nguyen on 2/9/20.
//  Copyright © 2020 drjoy. All rights reserved.
//

import Foundation
import RxDataSources

struct PostsSectionData {
    var header: String
    var items: [Post]
    var isExpanded: Bool = false
}
