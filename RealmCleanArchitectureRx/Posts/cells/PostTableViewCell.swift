import UIKit

final class PostTableViewCell: UITableViewCell {

    @IBOutlet weak var detailsLabel: UILabel!
    
    func bind(_ viewModel: Post) {
        self.detailsLabel.text = viewModel.body
    }
    
}
