//
//  ViewModelType.swift
//  SEED
//
//  Created by Ryne Cheow on 17/4/17.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import RxCocoa

protocol ViewModelType: class { }

protocol TrackableViewModelType { }

typealias TransformableViewModelType = ViewModelType & Transformable
