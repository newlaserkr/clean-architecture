//
//  PostsUseCase.swift
//  RealmCleanArchitectureRx
//
//  Created by drjoy on 2/6/20.
//  Copyright © 2020 drjoy. All rights reserved.
//

import Foundation
import RxSwift

protocol PostsUseCase {
    func posts() -> Observable<[Post]>
    func delete(_ post: Post) -> Observable<Void>
    func addPost(_ newPost: Post) -> Observable<Void>
}
