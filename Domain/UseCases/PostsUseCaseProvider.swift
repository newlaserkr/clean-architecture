//
//  PostsUseCaseProvider.swift
//  RealmCleanArchitectureRx
//
//  Created by drjoy on 2/6/20.
//  Copyright © 2020 drjoy. All rights reserved.
//

import Foundation

protocol PostsUseCaseProvider {
    
    func makePostsUseCase() -> PostsUseCase
}
