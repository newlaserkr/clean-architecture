//
//  RealmLiveScheduler.swift
//  External
//
//  Created by Ryne Cheow on 17/02/2018.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift
import RxRealm

public class RealmLiveScheduler: ImmediateSchedulerType {

   private var notificationRunLoop: CFRunLoop?

   public func schedule<StateType>(_ state: StateType,
                                   action: @escaping (StateType) -> Disposable) -> Disposable {
      let cancel = SingleAssignmentDisposable()

      // Execute on background thread
      DispatchQueue.global(qos: .background).async {
         self.notificationRunLoop = CFRunLoopGetCurrent()

         // Perform in current thread run loop
         CFRunLoopPerformBlock(self.notificationRunLoop,
                               CFRunLoopMode.defaultMode.rawValue) {
                                 if cancel.isDisposed {
                                    return
                                 }
                                 cancel.setDisposable(action(state))
         }
         CFRunLoopRun()
      }

      return cancel
   }

   deinit {
      if let runloop = notificationRunLoop {
         CFRunLoopStop(runloop)
      }
   }
}
